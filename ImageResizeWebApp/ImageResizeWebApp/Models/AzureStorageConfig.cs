﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.ComponentModel.DataAnnotations;

namespace ImageResizeWebApp.Models
{
    public class AzureStorageConfig
    {
        public string AccountName { get; set; }
        public string AccountKey { get; set; }
        public string QueueName { get; set; }
        public string ImageContainer { get; set; }
        public string ThumbnailContainer { get; set; }
   
        // This property will hold a state, selected by user
        [Required]
        [Display(Name = "")]
        public string Prefix { get; set; }

        // This property will hold all available prefixes for selection
        public IEnumerable<SelectListItem> Prefixes { get; set; }
    }
}
