using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using ImageResizeWebApp.Models;

namespace ImageResizeWebApp.Controllers
{
    // a global variable really to hold the selected prefix
    public class Vars
    {
        public static string Prefix = "a";
    }

    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var model = new AzureStorageConfig();
            // Let's get all prefixes that we need for a DropDownList
            var prefixes = GetAllPrefixes();

            // Create a list of SelectListItems so these can be rendered on the page
            model.Prefixes = GetSelectListItems(prefixes); 

            model.Prefix = Vars.Prefix;

            return View(model);
        }

        public IActionResult Error()
        {
            ViewData["RequestId"] = Activity.Current?.Id ?? HttpContext.TraceIdentifier;
            return View();
        }

        // Just return a list of prefixes - in a real-world application this would call
        // into data access layer to retrieve prefixes from a database.
        private IEnumerable<string> GetAllPrefixes()
        {
            return new List<string>
            {
                "a",
                "b",
                "c",
                "d",
                "e",
                "f",
                "g",
                "h",
                "i",
                "j",
                "k",
                "l",
                "m",
                "n",
                "o",
                "p",
                "q",
                "r",
                "s",
                "t",
                "u",
                "v",
                "w",
                "x",
                "y",
                "z",
            };
        }
        // This is one of the most important parts in the whole example.
        // This function takes a list of strings and returns a list of SelectListItem objects.
        // These objects are going to be used later in the SignUp.html template to render the
        // DropDownList.
        private IEnumerable<SelectListItem> GetSelectListItems(IEnumerable<string> elements)
        {
            // Create an empty list to hold result of the operation
            var selectList = new List<SelectListItem>();

            // For each string in the 'elements' variable, create a new SelectListItem object
            // that has both its Value and Text properties set to a particular value.
            // This will result in MVC rendering each item as:
            //     <option value="State Name">State Name</option>
            foreach (var element in elements)
            {
                selectList.Add(new SelectListItem
                {
                    Value = element + "_",
                    Text = element
                });
            }

            return selectList;
        }

        //
        // 2. Action method for handling user-entered data when 'Sign Up' button is pressed.
        //
        [HttpPost]
        public ActionResult SelectLetter(AzureStorageConfig model)
        {
            // I want to record the returned selection for later so that we can use it for teh 
            // uploaded filename prefix

            Vars.Prefix = model.Prefix;

            // Get all prefixes again
            var prefixes = GetAllPrefixes();

            var newmodel = new AzureStorageConfig();

            // Set these prefixes on the model. We need to do this because
            // only the selected value from the DropDownList is posted back, not the whole
            // list of prefixes
            newmodel.Prefixes = GetSelectListItems(prefixes);

            return View("Index",newmodel);
        }

        public ActionResult SelectLetter()
        {
            var prefixes = GetAllPrefixes();

            var newmodel = new AzureStorageConfig();

            // Set these prefixes on the model. We need to do this because
            // only the selected value from the DropDownList is posted back, not the whole
            // list of prefixes
            newmodel.Prefixes = GetSelectListItems(prefixes);

            return View(newmodel);            
        }
    }
}
